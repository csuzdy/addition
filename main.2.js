var R = 15;
var svgWidth = 8*2*R;
var pointsWidth = 500;
var padding = R*0.7;
var TOP = R;
var svgHeight = 20*R + 9*padding + TOP;
var color1 = '#1f77b4';
var color2 = '#ff7f0e';
var transitionDuration = 1000;
var leftCenterX = R;
var rightCenterX = svgWidth-R;
var result = 0;
var pointNodes = [];
var pointRadius = R*0.7;
var simulation;
var charge = false;
var level = 1;
var packman = false;


function draw(data) {
    console.log('draw: ', data);
    var svg = d3.select('#exercise svg');
    var circle = svg.selectAll('circle')
                    .data(data);

    circle.exit()
        .transition()
        .duration(transitionDuration)
        .attr('r', 0)
        .remove();

    circle.enter()
        .append('circle')
        .attr('cy', calculateCy)
        .attr('cx', calculateCx)
        .attr('r', R/10)
        .attr('fill', (d, i) => d.color)
        .transition()
        .duration(transitionDuration)
        .attr('r', R);

    circle.merge(circle)
        .transition()
        .duration(transitionDuration)
        .attr('cy', calculateCy)
        .attr('cx', calculateCx)
        .attr('fill', (d, i) => d.color);
}

function calculateCy(data) {
    switch (data.pos) {
        case 'left':
        case 'right':
            return data.i*(R*2+padding) + R + TOP;
        default:
            return R + TOP;
    }
}

function calculateCx(data) {
    switch (data.pos) {
        case 'left':
            return leftCenterX;
        case 'right':
            return rightCenterX;
        default:
            return data.i*(R*2+padding) + R;
    }
}

function generateData(left, right) {
    var data = [];
    for (let i = 0;i<left;i++) {
        data.push({
            pos: 'left',
            i: 9-i,
            color: getColor(i)
        });
    }
    for (let i = 0;i<right;i++) {
        data.push({
            pos: 'right',
            i: 9-i,
            color: getColor(i)
        });
    }
    return data;
}

function getColor(i) {
    if (i < 5) {
        return color1;
    } else {
        return color2;
    }
}

function updateText(left, right) {
    d3.select('#exercise svg')
      .select('text')
      .attr('fill', 'white')
      .html(left+ '&nbsp;+&nbsp;' + right)
      .transition()
      .duration(transitionDuration*2)
      .attr('fill', 'black');
}

function update(left, right) {
    updateText('', '');
    if (level >= 4) {
        draw([]);
    } else {
        draw(generateData(left, right));
    }
    updateText(left, right);
}

function init() {
    var textY = TOP + 10*R + padding*4;

    d3.select('#exercise svg')
      .attr('width', svgWidth)
      .attr('height', svgHeight);

    d3.select('#exercise svg')
      .select('text')
      .attr('x', 2*R + padding)
      .attr('y', 10*R + padding*4 + TOP)
      .attr('font-size', R*3.2);

    d3.select('#exercise svg')
      .select('line')
      .attr('x1', leftCenterX - R*2)
      .attr('x2', rightCenterX + R*2)
      .attr('y1', textY + padding/2)
      .attr('y2', textY + padding/2);

    pointsWidth = d3.select("#points")
                    .node()
                    .getBoundingClientRect()
                    .width;
    d3.select('#points svg')
      .attr('height', svgHeight)
      .attr('width', pointsWidth)
      .on('mousemove', pointsMouse)
      .on('click', pointsClick);

    d3.select('#points rect')
      .attr('height', svgHeight)
      .attr('width', pointsWidth)
      .attr('fill', 'yellow')
      .attr('fill-opacity', 0.5)
      .attr('rx', pointRadius);

    document.addEventListener('keypress', keyPress);
    updateRandom();
    initPointsSimulation();
}

function pointsMouse() {
    if (packman) {
        return;
    }
    var p1 = d3.mouse(this);
    var px = p1[0];
    var py = p1[1];
    simulation.force('radial', d3.forceRadial(R*2, px, py))
              .alpha(1)
              .restart();
}

function pointsClick() {
    if (packman) {
        return;
    }
    charge = !charge;
    if (charge) {
        simulation.force('charge', d3.forceManyBody().strength(-40).distanceMin(pointRadius).distanceMax(pointRadius*10))
                    .alpha(1)
                    .restart();
    } else {
        simulation.force('charge', null);
    }
}

function keyPress(e) {
    switch(e.key) {
        case 'p':
            packman = !packman;
            if (packman) {
                startPackman();
            } else {
                stopPackman();
            }
            break;
    }
}

function startPackman() {
    d3.select('#buttons')
      .selectAll('button')
      .attr('disabled', 'disabled');
}

function stopPackman() {
    d3.select('#buttons')
      .selectAll('button')
      .attr('disabled', null);
}

function updateButtons(answers) {
    console.log('answers: ', answers);
    var buttons = d3.select('#buttons')
                    .selectAll('button')
                    .data(answers);

    buttons.enter()
           .append('button')
           .attr('type', 'button')
           .attr('class', 'btn btn-primary btn-block')
           .text((d, i) => d)
           .on('click', buttonClick);

    buttons.exit()
           .remove();

    buttons.merge(buttons)
           .text((d, i) => d);
}

function updateRandom() {
    var left, right;
    if (level === 1) {
        left = getRandomInt(0, 3);
        right = getRandomInt(0, 3);
    } else if (level === 2) {
        left = getRandomInt(0, 5);
        right = getRandomInt(0, 5);
    } else if (level === 3 || level === 4) {
        left = getRandomInt(0, 10);
        right = getRandomInt(0, 10-left);
    } else if (level === 5) {
        left = getRandomInt(0, 20);
        right = getRandomInt(0, 20);
    } else {
        left = getRandomInt(0, 100);
        right = getRandomInt(0, 100);
    }
    result = left + right;
    if (level === 4) {
        let min = 0;
        let max = 10;

        let answers = getRandomItemsSorted(d3.range(min, max+1), 4, result);
        updateButtons(answers);
    } else if (level === 5) {
        let min = 0;
        let max = 40;

        let answers = getRandomItemsSorted(d3.range(min, max+1), 7, result);
        updateButtons(answers);
    } else if (level === 6) {
        let min = 0;
        let max = 200;

        let answers = getRandomItemsSorted(d3.range(min, max+1), 10, result);
        updateButtons(answers);
    } else {
        updateButtons(d3.range(11).reverse());
    }
    update(left, right);
}

function getRandomItemsSorted(arr, num, result) {
    let res =  d3.shuffle(arr)
                 .slice(0, num);
    if (res.includes(result) === false) {
        res.pop();
        res.push(result);
    }
    return res.sort(d3.descending);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function buttonClick(e) {
    if (result != e) {
        updatePoints(-level);
        d3.select('#exercise svg')
          .transition()
          .duration(transitionDuration/3)
          .style('background','red')
          .transition()
          .duration(transitionDuration/3)
          .style('background','');
    } else {
        updatePoints(1);
        updateRandom();
    }
}

function initPointsSimulation() {
    simulation = d3.forceSimulation()
                     .nodes(pointNodes)
                    .force('collide', d3.forceCollide().radius(pointRadius*1.1).iterations(2))
                    .force('gravity', d3.forceManyBody().strength(15).distanceMin(pointRadius).distanceMax(pointRadius*3))
                    .force('container', containerForce)
                    .on('tick', function() {
                        console.log('tick');
                        var svg = d3.select('#points svg');
                        var circle = svg.selectAll('circle')
                                        .data(pointNodes);
                        circle.exit()
                            .remove();

                        circle.enter()
                            .append('circle')
                            .attr('cy', d => d.y)
                            .attr('cx', d => d.x)
                            .attr('fill', d => d.color)
                            .transition()
                            .duration(transitionDuration/2)
                            .attr('r', d => d.r);

                        circle.merge(circle)
                            .attr('cy', d => d.y)
                            .attr('cx', d => d.x);
                    });
}

function containerForce(alpha) {
    for (let node of pointNodes) {
        if (node.x < pointRadius) {
            node.x = pointRadius;
        }
        if (node.x > pointsWidth-pointRadius) {
            node.x = pointsWidth-pointRadius;
        }
        if (node.y > svgHeight-pointRadius) {
            node.y = svgHeight-pointRadius;
        }
    }
}

function updatePoints(delta) {
    if (delta == 0) {
        return;
    }
    if (delta > 0) {
        for (let i = 0;i<delta;i++) {
            pointNodes.push({
                r: pointRadius,
                x: svgWidth/2,
                y: svgHeight-pointRadius,
                color: d3.schemeCategory10[level % 10]
            });
        }
    } else {
        pointNodes.splice(pointNodes.length+delta);
    }

    if (pointNodes.length > 60) {
        level = 6;
    } else if (pointNodes.length > 40) {
        level = 5;
    } else if (pointNodes.length > 30) {
        level = 4;
    } else if (pointNodes.length > 20) {
        level = 3;
    } else if (pointNodes.length > 10) {
        level = 2;
    } else {
        level = 1;
    }
    simulation.force('radial', null)
              .nodes(pointNodes)
              .alpha(1)
              .restart();
}

init();

